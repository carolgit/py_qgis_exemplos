import os
from qgis.core import Qgis

SRS_id = 'EPSG:3857'
HOST = '10.10.0.3'
PORT = '5432'
USER = 'postgres'
PASSWORD = 'postgres'
SCHEMA = 'public'
DB_NAME = 'plugin_verifier'

for layer in QgsProject.instance().mapLayers().values():
    if isinstance(layer, QgsVectorLayer):
        if layer.storageType() == 'ESRI Shapefile':
            if layer.crs().authid() == SRS_id:
                name_tb = layer.name()
                caracter_not_allowed_but_used = [caracter for caracter in [" ","@","#","%","-",".","(",")","*","<",">", ":",";","'","\"","/", "\\","+","=","!", "?", "|"] if caracter in name_tb]
                if len(caracter_not_allowed_but_used) > 0:
                    iface.messageBar().pushMessage("Error", "Usando caracteres não permitidos no nome do arquivo:" +str(caracter_not_allowed_but_used), level= Qgis.Critical)
                else:
                    if len(name_tb) < 45:
                        uri = layer.dataProvider().dataSourceUri()
                        command = f"""ogr2ogr -progress --config PG_USE_COPY YES --config SHAPE_ENCODING utf-8 -f PostgreSQL "PG:dbname='{DB_NAME}' host={HOST} port={PORT} user='{USER}' password='{PASSWORD}' sslmode=disable active_schema={SCHEMA}" -lco DIM=2 {uri} {name_tb} -overwrite -nlt POLYGON -lco GEOMETRY_NAME=wkb_geometry -lco FID=id -nln public.areas_urbanas -a_srs EPSG:3857 -nlt PROMOTE_TO_MULTI"""
                        print(command)
                        command = "ogr2ogr -progress --config PG_USE_COPY YES --config SHAPE_ENCODING utf-8 -f PostgreSQL \"PG:dbname='{DB_NAME}' host={HOST} port={PORT} user='{USER}' password='{PASSWORD}' sslmode=disable active_schema={SCHEMA}\" -lco DIM=2 {uri} {name_tb} -overwrite -nlt POLYGON -lco GEOMETRY_NAME=wkb_geometry -lco FID=id -nln {SCHEMA}.{name_tb} -a_srs {SRS_id} -nlt PROMOTE_TO_MULTI".format(SRS_id = SRS_id, HOST = HOST, PORT = PORT, USER = USER, PASSWORD = PASSWORD, SCHEMA = SCHEMA, DB_NAME = DB_NAME, uri=uri, name_tb=name_tb)
                        print(command)
                        print(os.system(command)) 
                    else:
                        iface.messageBar().pushMessage("Error", "Nome do arquivo espera no máximo 45 caracteres, mas obteve: " +str(len(name_tb)), level= Qgis.Critical)
                        
                    
